<?php

namespace App\Repository;

use App\Entity\Legalpages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Legalpages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Legalpages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Legalpages[]    findAll()
 * @method Legalpages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalpagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Legalpages::class);
    }


    
    // /**
    //  * @return Legalpages[] Returns an array of Legalpages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Legalpages
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
