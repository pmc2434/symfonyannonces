<?php

namespace App\Controller;


use App\Entity\Users;
use App\Entity\Images;
use App\Form\UsersType;
use App\Entity\Annonces;
use App\Form\ProfileType;
use App\Form\AnnoncesType;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/users", name="users_")
 */

class UsersController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('users/index.html.twig');
    }

    /**
     * @Route("/admin/list", name="admin_list")
     */
    public function usersList(UsersRepository $usersRepository): Response
    {
        return $this->render('users/list.html.twig', [
            'users' => $usersRepository->findAll(),
        ]);
    }


    /**
     * @Route("/annonces", name="annonces")
     */
    public function profil(): Response
    {
        return $this->render('users/annonces/profile.html.twig');
    }


    /**
     * @Route("/edit/profil", name="edit_profil")
     */
    public function editProfile(Request $request, ManagerRegistry $doctrine)
    {
        // j'appelle l'user connecté
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('message', "profil mis à jour");
            return $this->redirectToRoute('users_index');
        }
        // ici, je code le chemin de mon fichier DANS le dossier template
        return $this->render('users/editProfil.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/password", name="edit_password")
     */

    public function editPassword(Request $request, ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher): Response
    {
        if ($request->isMethod("POST")) {
            $em = $doctrine->getManager();

            // j'appelle l'user connecté
            $user = $this->getUser();
            if ($request->request->get('password1') == $request->request->get('password2')) {
                $user->setPassword($passwordHasher->hashPassword($user, $request->request->get('password1')));
                $em->flush();
                $this->addFlash('message', "mot de passe mis à jour");
                return $this->redirectToRoute('users_index');
            } else {
                $this->addFlash('error', "mdp différents");
            }
        }
        // ici, le rendu du html de la methode editPassword
        // ici, je code le chemin de mon fichier DANS le dossier template
        return $this->render('users/editPassword.html.twig');
    }


    /**
     * @Route("/new", name="new")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Users $user
     * @return Response
     */
    public function newUser(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword('azerty');
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('users_admin_list');
        }
        return $this->render('users/new_user.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit{", name="edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function editUser(Request $request, Users $user, EntityManagerInterface $entityManager): Response
    {

        $form = $this->createForm(UsersType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('users_admin_list');
        }
        return $this->renderForm('users/edit_user.html.twig', [
            'form' => $form,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"POST"})
     */
    public function deleteUser(Request $request, Users $user, EntityManagerInterface $entityManager, ManagerRegistry $doctrine): Response
    {
        if($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $doctrine->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('users_admin_list');

        
    }
}
