<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomController extends AbstractController
{
    /**
     * @Route("/custom", name="app_custom")
     */
    public function index(): Response
    {
        return $this->render('custom/index.html.twig', [
            'controller_name' => 'CustomController',
        ]);
    }



    // public function str_replace($val)
    // {
    //     // $comment = new Comments;
    //     foreach($_POST as $key => $val)	{
	// 		if(strstr(strtolower($val), "<script")) {
	// 			$val= str_replace("<script", "<em", strtolower($val));
	// 			$val= str_replace("</script>", "</em>", strtolower($val));
	// 		}
	// 		$val= str_replace("__APOSTROPHE__", "\"", $val);
	// 		$this->VARS_HTML[$key]= htmlspecialchars($val, ENT_QUOTES);
	// 	}




    // }


}
