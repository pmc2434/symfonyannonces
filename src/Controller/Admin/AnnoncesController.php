<?php

namespace App\Controller\Admin;


use App\Entity\Images;
use App\Entity\Annonces;
use App\Repository\AnnoncesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/admin/annonces", name="admin_annonces_")
 * @package App\Controller\Admin
 */


class AnnoncesController extends AbstractController
{
    /**
     * @Route("/", name="home")    
     * 
     */
    public function index(AnnoncesRepository $annoncesRepo): Response
    {
        return $this->render('admin/annonces/index.html.twig', [
            'annonces' => $annoncesRepo->findAll(),
        ]);        
    }

    
    /**
     * @Route("/activer/{id}", name="activer")    
     * 
     */
    public function activer(Annonces $annonce, ManagerRegistry $doctrine)
    {
        // en ajax 
        $annonce->setActive(($annonce->getActive()) ? false : true);

        $em = $doctrine->getManager();
        // persist = ajouter
        $em->persist($annonce);
        $em->flush();
        return new Response("true");
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")    
     * 
     */
    public function supprimer(Annonces $annonce, ManagerRegistry $doctrine)
    {
        // en ajax 
        // $annonce->setActive(($annonce->getActive())? false : true);

        $em = $doctrine->getManager();
        // persist = ajouter
        $em->remove($annonce);
        $em->flush();

        $this->addFlash('annonce', "'annonce' {{annonce.title}} 'supprimée'");
        return $this->redirectToRoute('admin_annonces_home');
    }

}
