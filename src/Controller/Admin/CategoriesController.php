<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use App\Form\CategoriesType;
use App\Repository\CategoriesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/admin/categories", name="admin_categories_")
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 */


class CategoriesController extends AbstractController
{
    /**
     * @Route("/", name="home")    
     * 
     */
    public function index(CategoriesRepository $catsRepo): Response
    {
        return $this->render('admin/categories/index.html.twig', [
            'categories' => $catsRepo->findAll(),
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutCategorie(Request $request, ManagerRegistry $doctrine)
    {
        $categorie = new Categories;
        $form = $this->createForm(CategoriesType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $doctrine->getManager();
            // persist = ajouter
            $em->persist($categorie);
            // flush = pousser en BDD
            $em->flush();

            return $this->redirectToRoute('admin_categories_home');
        }

        return $this->render('admin/categories/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }


    
    /**
     * @Route("/modifier/{id}", name="modifier")
     */
    public function modifCategorie(Categories $categorie, Request $request, ManagerRegistry $doctrine)
    {
        // le Categories ci-dessus est récupére par l'id ligne 63
        
        $form = $this->createForm(CategoriesType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $doctrine->getManager();
            // persist = ajouter
            $em->persist($categorie);
            // flush = pousser en BDD
            $em->flush();

            return $this->redirectToRoute('admin_categories_home');
        }

        return $this->render('admin/categories/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
