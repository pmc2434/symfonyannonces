<?php
namespace App\Controller;

use DateTime;
use App\Entity\Images;
use App\Entity\Annonces;
use App\Entity\Comments;
use App\Form\AnnoncesType;
use App\Form\CommentsType;
use App\Form\AnnonceContactType;
use App\Repository\AnnoncesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;


/**
 * @Route("/annonces", name="annonces_")
 */

class AnnoncesController extends AbstractController
{
    /**
     * @Route("/new", name="new")
     * @IsGranted("ROLE_USER")
     */
    public function ajoutAnnonce(Request $request, ManagerRegistry $doctrine): Response
    {
        $annonce = new Annonces;
        $form = $this->createForm(AnnoncesType::Class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // on récupère les images transmises depuis le formulaire
            $images = $form->get('images')->getData();
            //  on boucle sur les images (cas de plusieurs images)
            foreach($images as $image){
                // on genere un nouveau nom de fichier avec la même extension
                $fichier = md5(uniqid()) . '.' . $image->guessExtension(); 
                // on copie le fichier dans le dossier uploads :
                // images_directory a été parmétré ds le fichier services.yaml
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );  
                // on stocke l'image (son nom url) ds la BDD
                // on crée une nouvelle  instance de notre image
                $img = new Images();
                $img->setName($fichier);
                $annonce->addImage($img);
            }

            // important : on appelle '$this->getUser()' poour appeler l'user connecté
            // sans s à getUser (natif de la methode app_user)
            $annonce->setUsers($this->getUser());
            $annonce->setActive(true);
            $em = $doctrine->getManager();

            $em->persist($annonce);
            $em->flush();
            if($this->getUser()->hasRole('ROLE_USER') ){
                return $this->redirectToRoute('users_annonces');

            }else {
                return $this->redirectToRoute('admin_annonces_home');

            }
        }

        // ici, je code le chemin de mon fichier DANS le dossier template
        return $this->render('users/annonces/ajout.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function editAnnonce(Request $request, Annonces $annonce, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(AnnoncesType::Class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
             // on récupère les images transmises depuis le formulaire
             $images = $form->get('images')->getData();
             //  on boucle sur les images (cas de plusieurs images) 
             foreach($images as $image){
                 // on genere un nouveau nom de fichier avec la même extension
                 $fichier = md5(uniqid()) . '.' . $image->guessExtension(); 
                 // on copie le fichier dans le dossier uploads :
                 // images_directory  a été paramétré ds le fichier services.yaml
                 $image->move(
                     $this->getParameter('images_directory'),
                     $fichier
                 );  
                 // on stocke l'image (son nom url) ds la BDD
                 // on crée une nouvelle  instance de notre image
                 $img = new Images();
                 $img->setName($fichier);
                 $annonce->addImage($img);
                }            
            // important : on appelle '$this->getUser()' poour appeler l'user connecté
            // sans s à getUser (natif de la methode app_user)
            // $annonce->setUsers($this->getUser());
            $annonce->setActive(true);
            $em = $doctrine->getManager();

            $em->persist($annonce);
            $em->flush();
            if($this->getUser()->hasRole('ROLE_USER') ){
                return $this->redirectToRoute('users_annonces');
            }else {
                return $this->redirectToRoute('admin_annonces_home');
            }
        }
        // ici, je code le chemin de mon fichier DANS le dossier template
        return $this->render('users/annonces/edit.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView(),
        ]);
    }


    
    /**
     * @Route("/supprime/image/{id}", name="delete_image", methods={"DELETE"})    
     * @IsGranted("ROLE_USER")
     */
    public function deleteImage(Images $image, Request $request, ManagerRegistry $doctrine)
    {
        // tuto HS upload d'images multiples avec Symphonie Nouvelle Techno 48 mn
        // cette methode récupère les infos de l'entité Image afin de pouvoir la supprimer lors de l'édition/modification d'une annonce
        // par sécurité, on mets un token d'utilisation
        // on récupére les données en json (toujours avec Ajax) et on les décode
        // dans la request, on récupère le contenu avec $request->getContent()
        // true, c'est pour l'associatif (on récupère le nom des colonnes)
        $data = json_decode($request->getContent(), true);
        
        // on vérifie si le token est valide avec la méthode isCsrfTokenValid
        // le token que l'on reçoit doit avoir dElete et le n° d'image
        // on va chercher le nom de l'image
        
        if ($this->isCsrfTokenValid('delete'.$image->getId(), $data['_token'])) {
            // on récupère le nom de l'image
            $nom = $image->getName();
            // on supprime le fichier correspondant
            // images_directory  a été parmétré ds le fichier services.yaml
            unlink($this->getParameter('images_directory') . '/' . $nom);

            // on supprime l'entrée dans la base
            $em = $doctrine->getManager();
            $em->remove($image);
            $em->flush();
            
            // on répond en json
            return new JsonResponse(['success' => 1]);        
        }
        else{
            return new JsonResponse(['error' => 'Token Invalide'], 400);
            
        }
    }
    
        
    /**
     * @Route("/details/{slug}", name="details")
     */
    public function detail($slug, AnnoncesRepository $annoncesRepository, Request $request, MailerInterface $mailer, ManagerRegistry $doctrine): Response
    {
        // findOneBy parce que l'on veut une annonce
        $annonce = $annoncesRepository->findOneBy(['slug' => $slug]);
    
        if (!$annonce) {
            throw new NotFoundHttpException("pas d'annonce trouvée");
            // throw new NotFoundHttpException('pas d\'annonce trouvée');
        }        
        // dd($annonce);    
        // ici, le code pour appeller et traiter le formulaire de contact    
        $form = $this->createForm(AnnonceContactType::class);
        $contact = $form->handleRequest($request);

        // ici, la gestion du formulaire de contact pour les annonces     
        if ($form->isSubmitted() && $form->isValid()) {
            $email = (new TemplatedEmail())
                // on récupére les infos de la personne qui a envoyé le formulaire de contact
                ->from($contact->get('email')->getData())
                // on l'envoie à qui :
                // dans l'entité Users, je récupère le mail
                ->to($annonce->getUsers()->getEmail())
                ->subject('contact au sujet de votre annonce"' . $annonce->getTitle() . '"')
                ->htmlTemplate('emails/contact_annonce.html.twig')
                ->context([
                    'annonce' => $annonce, 
                    'mail' => $contact->get('email')->getData(),
                    'message'=> $contact->get('message')->getData(),
                ]);
                // j'envoie le mail
                try {
                    $mailer->send($email);
                } catch(TransportExceptionInterface $e) {
                    // some error prevented the email sending; display an
                    // error message or try to resend the message
                }
    
                // on confirme et on redirige contact
                $this->addFlash('message', 'Votre e-mail a été envoyé');
                return $this->redirectToRoute('annonces_details', ['slug'=>$annonce->getSlug()])
                ;
        }


        // *************************************************************start CODE COMMENTAIRE**************************
        // ici, j'ajoute le code correspondant à l'ajout d'un commentaire.
        // je crée le com vierge en instanciant l'objet :
        $comment = new Comments;

        // on génére le formulaire :
        $commentForm = $this->createForm(CommentsType::class, $comment);
        // on fait un handleRequest sur la variable $request afin de récupérer les différents champs
        $commentForm->handleRequest($request);

        // traitement du formulaire:
        if($commentForm->isSubmitted() && $commentForm->isValid()){
            // $comment->setCreatedAt(new DateTime());
            $comment->getCreatedAt(new DateTime());
            
            // on a récupérer $annonce ligne 178
            $comment->setAnnonces($annonce);
            // dd($comment);
            // on recupére parentid dans le formulaire CommentsType ligne 46
            $parentid = $commentForm->get("parentid")->getData();

            // dd($parentid);

            // on supprime l'entrée dans la base
            $em = $doctrine->getManager();
            // on va chercher le com correspondant:
            // soit on récupère null, soit on récupère le com
            if($parentid != null){
                $parent = $em->getRepository(Comments::class)->find($parentid);
            }
            // on définit le parent :
            $comment->setParent($parent ?? null);
            $em->persist($comment);
            $em->flush();

            $this->addFlash('message', 'commentaire envoyé');
            return $this->redirectToRoute('annonces_details', ['slug' => $annonce->getSlug()]);
            
        }
        

        //  la ligne commentée correspond au code de la page détail de l'annonce
        // return $this->render('annonces/details.html.twig',compact('annonce'));
        // la deuxième correspondant au code aprés le codage du formulaire contact de l'annonce (Tuto 9 14 mn)
        // j'envoie le détail de ma vue dans le formulaire de contact de l'annonce
        // on ne peux pas utiliser "compact" avec CreateView
        return $this->render('annonces/details.html.twig', [
            'annonce' => $annonce, 
            'form' => $form->createView(),
            // je rajoute paramétre ds le render (vue) pour les commentaires :
            'commentform' => $commentForm->createView(),
        ]);
    }
}
