<?php

namespace App\Controller;

use App\Entity\Legalpages;
use App\Form\LegalpagesType;
use App\Repository\LegalpagesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/legalpages")
 * 
 */
class LegalpagesController extends AbstractController
{
    /**
     * @Route("/", name="legalpages_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(LegalpagesRepository $legalpagesRepository): Response
    {
        return $this->render('legalpages/index.html.twig', [
            'legalpages' => $legalpagesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="legalpages_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $legalpage = new Legalpages();
        $form = $this->createForm(LegalpagesType::class, $legalpage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($legalpage);
            $entityManager->flush();

            return $this->redirectToRoute('legalpages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('legalpages/new.html.twig', [
            'legalpage' => $legalpage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="legalpages_show", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(Legalpages $legalpage): Response
    {
        return $this->render('legalpages/show.html.twig', [
            'legalpage' => $legalpage,
        ]);
    }
    /**
     * @Route("/pages/{id}", name="legalpages_pages", methods={"GET"})
     */
    public function pages(Legalpages $legalpage): Response
    {
        return $this->render('legalpages/pages.html.twig', [
            'legalpage' => $legalpage,
           
        ]);
    }

    /**
     * @Route("/{id}/edit", name="legalpages_edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Legalpages $legalpage, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(LegalpagesType::class, $legalpage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('legalpages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('legalpages/edit.html.twig', [
            'legalpage' => $legalpage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="legalpages_delete", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Legalpages $legalpage, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$legalpage->getId(), $request->request->get('_token'))) {
            $entityManager->remove($legalpage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('legalpages_index', [], Response::HTTP_SEE_OTHER);
    }
}
