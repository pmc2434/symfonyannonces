<?php

namespace App\Form;

use App\Entity\Comments;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CommentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email : ',
                'attr' => [
                    // form-control est la classe Axentix
                    'class' => 'form-control',
                ]
            ])
            ->add('nickname', TextType::class, [
                'label' => 'Pseudo : ',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('content', TextType::class, [
                'label' => 'Votre commentaire  : ',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])

            ->add('rgpd', CheckboxType::class)

            // on va stocker ds un input invisible le champ du commentaire auquel on répond
            // on le récupérera ds AnnoncesController ligne 231
            // comme il n'existe pas dans la BDD, on lui met la propriété mapped à false.
            // il n'est donc pas mappé, il faudra lui faire un bout de code à l'intérieur de AnnoncesControlleur
            ->add('parentid', HiddenType::class, [
                'mapped' => false,
            ])
            // ->add('envoyer', SubmitType::class)
            ->add('Envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn rounded-1 primary press mt-1 mb-3',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comments::class,
        ]);
    }
}
