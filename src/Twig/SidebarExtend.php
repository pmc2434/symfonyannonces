<?php

namespace App\Twig;

use Twig\Environment;
use Twig\TwigFunction;
use App\Entity\Legalpages;
use Twig\Extension\AbstractExtension;
use App\Repository\LegalpagesRepository;

class SidebarExtend extends AbstractExtension
{
    public function __construct(LegalpagesRepository $legalpagesRepository)
    {
        $this->legalpagesRepository = $legalpagesRepository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction(
                'renderLegal',
                [$this, 'renderLegal'],
                ['needs_environment' => true]),
        ];
    }

    public function renderLegal(Environment $environment)
    {        
        return $environment->render('_partials\_sidebar.html.twig', [
            'legalpages' => $this->legalpagesRepository->findAll(),
        ]);
    }
}