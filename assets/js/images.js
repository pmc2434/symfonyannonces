window.onload = () => {
  // gestion des liens "Supprimer"
  // on récupére les liens ayant l'attribut datadelete dans users/annonces/edit.html.twig
  let links = document.querySelectorAll("[data-delete]");
//   tuto 1h03mn
  console.log(links);
  // on boucle sur links pour éviter de faire un compteur(équivalent de foreach en php)
  for (link of links) {
    // on écoute le click sur link
    link.addEventListener("click", function (e) {
      // on empeche la navigation vers le href
      e.preventDefault();
      // on demande confirmation de la suppression
      if (confirm("Voulez-vous supprimer cette belle image ?")) {
        //   on envoie une requête Ajax vers le href du lien avec la méthode DELETE (tuto HS upload image 1h 10mn)
        fetch(this.getAttribute("href"), {
            method: "DELETE",
            headers: {
                'X-Requested-With': "XMLHttpRequest",
                "Content-Type": "application/json"
                },
            // on envoie un tableau (body) que l'on convertit en json
            // this est le lien sur lequel on a cliqué
            body: JSON.stringify({"_token": this.dataset.token})
        }).then(
            // on récupère la réponse en json
            response => response.json()

        ).then(data => {
            if(data.success)
                this.parentElement.remove()
                else
                alert(data.error)
        }).catch(e => alert(e))
      }
    });
  }
};
